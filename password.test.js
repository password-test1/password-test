const { checkLength, checkAlphabet, checkDigit, checkSymbol, checkPassword } = require('./password')
describe('Test Password Length', () => {
  test('should 8 characters to be true', () => {
    expect(checkLength('12345678')).toBe(true)
  })

  test('should 7 characters to be false', () => {
    expect(checkLength('1234567')).toBe(false)
  })

  test('should 25 characters to be true', () => {
    expect(checkLength('1234567890123456789012345')).toBe(true)
  })

  test('should 26 characters to be false', () => {
    expect(checkLength('12345678901234567890123456')).toBe(false)
  })
})

describe('Test Alphabet', () => {
  test('should has Alphabet in password to be true', () => {
    expect(checkAlphabet('a')).toBe(true)
  })

  test('should has not Alphabet in password to be false', () => {
    expect(checkAlphabet('12345')).toBe(false)
  })

  test('should has Alphabet UpperCase in password to be true', () => {
    expect(checkAlphabet('A')).toBe(true)
  })
})

describe('Test Digit', () => {
  test('should has Digit in password to be true', () => {
    expect(checkDigit('1')).toBe(true)
  })

  test('should has not Digit in password to be false', () => {
    expect(checkDigit('abcd')).toBe(false)
  })
})

describe('Test Symbol', () => {
  test('should has Symbol in password to be true', () => {
    expect(checkSymbol('@123456789')).toBe(true)
  })

  test('should has Symbol ! in password to be true', () => {
    expect(checkSymbol('123456789!')).toBe(true)
  })

  test('should has not Symbol in password to be false', () => {
    expect(checkSymbol('12345678')).toBe(false)
  })
})

describe('Test Password', () => {
  test('should password @Ben1234 to be true', () => {
    expect(checkPassword('@Ben1234')).toBe(true)
  })

  test('should password @Ben123 to be false', () => {
    expect(checkPassword('@Ben123')).toBe(false)
  })

  test('should password benjamasK@ to be false', () => {
    expect(checkPassword('benjamasK@')).toBe(false)
  })

  test('should password #Benjamas123456789012345678 to be false', () => {
    expect(checkPassword('#Benjamas123456789012345678 to be false')).toBe(false)
  })
})
